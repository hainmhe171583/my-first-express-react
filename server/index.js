var express = require('express');
const cors = require('cors');
var app = express();

app.use(cors());
app.use(require('./routes'));


const port = 8080;
app.listen(port, '127.0.0.1', () => {
    console.log(`Example app listening on port ${port}`)
});