var knex = require('../knexdb');
var express = require('express');
var app = express();
var cors = require('cors');
var bodyparser = require('body-parser');
var token = require('./token');


app.use(cors());
app.use(bodyparser.json());





function newUser(req, res, username, password) {
    knex('user').where({
        username: username
    }).select('username').then(
        (row) => {
            console.log(row);
            if (row.length == 0) {
                    
                    knex.insert([
                        {username:username, password:password}
                    ]).into('user').catch(function(err){
                    console.log(err);
                });


                    let tokenString = token(req, res);

                    var date = new Date();
                    date.setDate(date.getDate() + 1)
                    res.send({
                        authsuccess: true,
                        tokenString: tokenString,
                        expires: date.toISOString().replace(/T/, ' ').replace(/\..+/, ''),
                        username: username
                    });
            }
            else {
                res.send({
                    authsuccess: false,
                    message: "Username existed!!"
                });
            }
        }

    );
}



app.post('/', (req, res) => {
    console.log("submited");
    let { username, password } = req.body;
    //console.log(username);
    newUser(req, res, username, password);

    //res.json("submited")
}
);


app.get('/', (req, res) =>
    res.send("/api/register"));

module.exports = app;