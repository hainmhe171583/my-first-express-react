var express = require('express');
var routes = express.Router();
var {checkLogin} = require('./logincheck');
var loginRouter = require('./login');
var registerRouter = require('./register');
var checktokenRouter = require('./checktoken');


// routes.use('/logincheck', checkLogin());
routes.use('/login', loginRouter);
routes.use('/register', registerRouter);
routes.use('/checktoken', checktokenRouter)
//routes.use('/token',require('./token'))

//routes.get('/get',(req,res)=>res.send("dsadsa"))
module.exports = routes;


