var express = require('express');
var routes = express.Router();
var listworkRouter = require('./list.work');
var editworkRouter = require('./edit.work');
var newworkRouter = require('./new.work');
var deleteworkRouter = require('./delete.work');

routes.use('/listwork', listworkRouter);
routes.use('/editwork', editworkRouter);
routes.use('/newwork', newworkRouter);
routes.use('/deletework', deleteworkRouter);


module.exports = routes;