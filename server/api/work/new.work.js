var knex = require('../knexdb');
var express = require('express');
var app = express();
var cors = require('cors');
var bodyparser = require('body-parser');
var jwt = require("jsonwebtoken");


app.use(cors());
app.use(bodyparser.json());

function newwork(req, res) {
    let { userId, title, description } = req.body;
    knex.insert([
        {
            userId: userId,
            title: title,
            description: description
        }
    ]).into('work').catch(function (err) {
        console.log(err);
    });
    res.send({
        authsuccess: true,
        Status: true
    });
}

app.post('/', (req, res) => {
    console.log('submited for new work!!');
    newwork(req, res);
});

module.exports = app;