var knex = require('../knexdb');
var express = require('express');
var app = express();
var cors = require('cors');
var bodyparser = require('body-parser');
var jwt = require("jsonwebtoken");


app.use(cors());
app.use(bodyparser.json());

function deletework(req, res) {
    let { Id } = req.body;
    //console.log(Id);
    
    knex('work')
        .where('Id',Id)
        .del()
        .catch(
        function (err) {
            console.log(err)
        });
        
    res.send({
        authsuccess: true,
        Status: true
    });
}

app.post('/', (req, res) => {
    console.log('submited for delete a work!!');
    deletework(req, res);
});

module.exports = app;