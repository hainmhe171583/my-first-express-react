var express = require('express');
var routes = express.Router();

var apiRouter = require('./api');


routes.use('/api', apiRouter);


module.exports = routes;


