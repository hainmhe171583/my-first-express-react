import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import React, { useEffect, useState } from 'react';



function Newwork() {
    const [title, setTitle] = useState('');
    const [description, setDescription] = useState('');

    function sendData(event) {
        event.preventDefault();
        var workDetail = {
            userId: localStorage.userId,
            title: title,
            description: description
        }
        fetch("http://127.0.0.1:8080/api/work/newwork", {
            method: 'post',
            headers: {
                "Content-Type": "application/json",
                "tokenString" : localStorage.getItem('tokenString'),
                "expires" : localStorage.getItem('expires')
            },
            body: JSON.stringify(workDetail)
        }).then(
            response => response.json()
        ).then(
            data => {

                console.log(JSON.stringify(data));
                if (data.authsuccess == false) {
                    localStorage.removeItem('tokenString');
                    //console.log(localStorage.getItem('tokenInfo'));
                    window.location.href = '/login';
                }
                window.location.href = '/mylist';
            })

        console.log("click");
    }

    return (
        <Form onSubmit={sendData} style={{
            width: "60%",
            background: "#d9d2c28a",
            padding: "40px",
            margin: "0px auto",
        }}>
            <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                <Form.Label>Your work title</Form.Label>
                <Form.Control type="text" placeholder="add your work tittle" onChange={e => setTitle(e.target.value)} />
            </Form.Group>
            <Form.Group className="mb-3" controlId="exampleForm.ControlTextarea1">
                <Form.Label>Your detail work</Form.Label>
                <Form.Control as="textarea" rows={3} onChange={e => setDescription(e.target.value)} />
            </Form.Group>
            <Button variant="primary" type="submit">
                Submit
            </Button>
        </Form>
    );
}

export default Newwork;