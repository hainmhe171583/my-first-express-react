import React, { useEffect, useState } from 'react';
import Listwork from './listwork';


function Home() {
    const [loginCheck, setLoginCheck] = useState(true);
    useEffect(
        () => {
            if (localStorage.getItem('tokenString'))
                setLoginCheck(true);
            else
                setLoginCheck(false);
        },
        [loginCheck]);

    if (loginCheck == true)
        return (
            <>
                <Listwork />
            </>
        );
}

export default Home;