import React, { useEffect, useState } from 'react';
import Table from 'react-bootstrap/Table';
import Modal from 'react-bootstrap/Modal';
import { Outlet, Link } from "react-router-dom";
import Button from 'react-bootstrap/Button';
import '../assets/listwork.css';
import Detail from '../modal/popup.detail.work';


function Listwork() {
    const [loginCheck, setLoginCheck] = useState(true);
    useEffect(
        () => {
            if (localStorage.getItem('tokenString'))
                setLoginCheck(true);
            else
                setLoginCheck(false);
        },
        [loginCheck]);

    const [dataset, setDataset] = useState([]);
    useEffect(() => {
        const token = localStorage.getItem('tokenString')
        console.log('tokennnnnn', token)
        fetch("http://127.0.0.1:8080/api/work/listwork", {

            headers: {
                "Content-Type": "application/json",
                "tokenString": token,
                "expires": localStorage.getItem('expires')
            }
            //body: localStorage.getItem('tokenInfo')
        })
            .then(function (res) {
                return res.json();

            })
            .then(function (data) {
                console.log(data);
                setDataset(data);
            })
            .catch(
                e => console.log(e)
            );
    }, []);

    function deleteWork(Id) {
        fetch("http://127.0.0.1:8080/api/work/deletework", {
            method: 'post',
            headers: {
                "Content-Type": "application/json",
                "tokenString": localStorage.getItem('tokenString'),
                "expires": localStorage.getItem('expires')
            },
            body: JSON.stringify({ Id: Id })
        })
            .then(function (res) {
                return res.json();

            })
            .then(function (data) {
                console.log(data);
                if (data.authsuccess == false) {
                    localStorage.removeItem('tokenString');
                    //console.log(localStorage.getItem('tokenInfo'));
                    window.location.href = '/login';
                }
                window.location.href = '/mylist';
            })
            .catch(
                e => console.log(e)
            );
    }


    
    if (loginCheck == true)
        return (
            <>
                <Table striped bordered hover variant="dark" style={{ width: "80%", margin: "0px auto" }}>
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Title</th>
                            <th style={{ width: "10%" }}>Option</th>

                        </tr>
                    </thead>
                    <tbody>
                        {
                            dataset.map((data, index) => {

                                return <tr key={data.Id}>

                                    <td>{index + 1}</td>
                                    <td>{data.title}</td>
                                    <td style={{ display: "flex" }}>
                                        <Detail var data={data} />&nbsp;
                                        {/* <Button variant="primary"><Link to="/" className="nav-link"> DETAIL </Link></Button>&nbsp;
                                <Button variant="primary"><Link to="/" className="nav-link"> EDIT </Link></Button>&nbsp;*/}
                                        <Button variant="primary" onClick={() => deleteWork(data.Id)}> DELETE </Button>
                                    </td>
                                </tr>
                            })}
                    </tbody>
                </Table>
            </>
        );
    else
        window.location.href = '/login';

}

export default Listwork;