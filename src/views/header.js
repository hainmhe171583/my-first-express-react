import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import NavDropdown from 'react-bootstrap/NavDropdown';

import React, { useEffect, useState } from 'react';
import ReactDOM from 'react-dom/client';
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Loggedheader from './loggedheader';
import LogOutHeader from './logoutheader';




function Header() {
    const [loginCheck, setLoginCheck] = useState(true);
    useEffect(
        () => {
            if(localStorage.getItem('tokenString'))
                setLoginCheck(true);
            else
                setLoginCheck(false);
            // fetch("http://127.0.0.1:8080/api/auth/logincheck", {

            //     headers: {
            //         "Content-Type": "application/json",
            //         "tokenString" : localStorage.getItem('tokenString'),
            //         "expires" : localStorage.getItem('expires')
            //     }
            //     //body: localStorage.getItem('tokenInfo')
            // })
            //     .then(function (res) {
            //         return res.json();

            //     })
            //     .then(function (data) {
            //         //console.log(data);
            //         if (data == true)
            //             setLoginCheck(true);
            //         else
            //             setLoginCheck(false);
            //     })
            //     .catch(
            //         e => console.log(e)
            //     );

        },
        [loginCheck]);
    if (loginCheck == true)
        return (
            <Loggedheader />
        );
    else
        return (<LogOutHeader />);
}

export default Header;